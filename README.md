# Docker image for Gitolite

This image allows you to run a git server in a container with OpenSSH and [Gitolite](https://github.com/sitaramc/gitolite#readme).

Based on Alpine Linux.
Forked from
[jgiannuzzi/gitolite](https://hub.docker.com/r/jgiannuzzi/gitolite),
[MrFreezeex/gitolite](https://hub.docker.com/r/MrFreezeex/gitolite),
and modified so that `sshd` options are handled better.

## Quick setup

1. Create volumes for your SSH server host keys and for your Gitolite config and repositories

        docker volume create --name gitolite-sshkeys
        docker volume create --name gitolite-git

2. Setup Gitolite with yourself as the administrator:

        docker run --rm -e SSH_KEY="$(cat ~/.ssh/id_rsa.pub)" -e SSH_KEY_NAME="$(whoami)" -v gitolite-sshkeys:/etc/ssh/keys -v gitolite-git:/var/lib/git registry.gitlab.com/gi1242/gitolite true

3. Finally run your Gitolite container in the background:

        docker run -d --name gitolite -p 22:22 -v gitolite-sshkeys:/etc/ssh/keys -v gitolite-git:/var/lib/git registry.gitlab.com/gi1242/gitolite

4. You can then add users and repos by following the [official guide](https://github.com/sitaramc/gitolite#adding-users-and-repos).

## Source repository

* https://gitlab.com/gi1242/docker-gitolite
* https://github.com/jgiannuzzi/docker-gitolite.git (upstream)
* https://github.com/MrFreezeex/docker-gitolite.git (upstream)
